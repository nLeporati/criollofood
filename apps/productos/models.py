from django.db import models
import json

# Create your models here.
class UnidadMedida(models.Model):
    nombre = models.CharField(max_length=50, unique=True)
    abreviatura = models.CharField(max_length=5, unique=True)

    class Meta:
        db_table = 'PRODUCTOS_UNIDAD_MEDIDA'
        verbose_name = "Unidad de medida"
        verbose_name_plural = "Unidades de medida"

    def __str__(self):
        return self.nombre

class Categoria(models.Model):
    nombre = models.CharField(max_length=100, unique=True)
    esta_en_carta = models.BooleanField()

    def __str__(self):
        return self.nombre

class Producto(models.Model):
    nombre = models.CharField(max_length=150, unique=True)
    cantidad_stock = models.PositiveIntegerField(db_column='CANTIDAD_STOCK')
    fecha_ingreso = models.DateTimeField(auto_now=True, db_column='FECHA_INGRESO')
    es_venta_individual = models.BooleanField(db_column='ES_VENTA_INDIVIDUAL')
    punto_pedido = models.PositiveIntegerField(db_column='PUNTO_PEDIDO')
    stock_minimo = models.PositiveIntegerField(db_column='STOCK_MINIMO')
    stock_maximo = models.PositiveIntegerField(db_column='STOCK_MAXIMO')
    precio_venta = models.DecimalField(max_digits=9, decimal_places=2, db_column='PRECIO_VENTA')
    precio_compra = models.DecimalField(max_digits=9, decimal_places=2, db_column='PRECIO_COMPRA')
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    unidad_medida = models.ForeignKey(UnidadMedida, on_delete=models.CASCADE, db_column='UNIDAD_MEDIDA_ID')
    imagen = models.ImageField(upload_to='productos')

    def __str__(self):
        return self.nombre

    @property
    def json(self):
        return json.dumps(self)

class Receta(models.Model):
    nombre = models.CharField(max_length=100, unique=True)
    descripcion = models.CharField(max_length=255)
    fecha_ingreso = models.DateTimeField(auto_now=True, db_column='FECHA_INGRESO')
    precio = models.DecimalField(max_digits=9, decimal_places=2)
    esta_disponible = models.BooleanField(default=True, db_column='ESTA_DISPONIBLE')
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    imagen = models.ImageField(upload_to='recetas')
    productos = models.ManyToManyField(Producto)
    tiempo_preparacion = models.PositiveIntegerField(db_column='TIEMPO_PREPARACION')

    def __str__(self):
        return self.nombre

    @property
    def json(self):
        return json.dumps(self)