from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User, Cliente, Mesa

# Register your models here.
admin.site.register(User, UserAdmin)
admin.site.register(Cliente)
admin.site.register(Mesa)