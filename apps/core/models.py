from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class User(AbstractUser):
    pass

class Cliente(models.Model):
    nombre = models.CharField(max_length=100)
    telefono = models.IntegerField(null=True)
    correo = models.CharField(max_length=255, unique=True, null=True)

    def __str__(self):
        return self.nombre

class Mesa(models.Model):
    numero_mesa = models.PositiveIntegerField(primary_key=True, db_column='NUMERO_MESA')
    en_uso = models.BooleanField(db_column='EN_USO')
    asientos = models.IntegerField()

    def __str__(self):
        return 'N°' + str(self.numero_mesa)