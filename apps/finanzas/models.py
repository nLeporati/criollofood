from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType

# Create your models here.
class MedioPago(models.Model):
    nombre = models.CharField(max_length=100, unique=True)
    esta_activo = models.BooleanField(default=True, db_column='ESTA_ACTIVO')

    class Meta:
        verbose_name = "Medio de pago"
        verbose_name_plural = "Medios de pago"

    def __str__(self):
        return self.nombre

class Caja(models.Model):
    numero_caja = models.PositiveIntegerField(primary_key=True, db_column='NUMERO_CAJA')
    esta_abierta = models.BooleanField(db_column='ESTA_ABIERTA')
    cajero = models.ForeignKey('core.User', on_delete=models.CASCADE)

    def __str__(self):
        return self.numeroCaja

class Pago(models.Model):
    monto = models.DecimalField(max_digits=9, decimal_places=2)
    fecha = models.DateTimeField(auto_now=True, db_column='FECHA_INGRESO')
    medio_pago = models.ForeignKey(MedioPago, on_delete=models.CASCADE, db_column='MEDIO_PAGO_ID')
    numero_caja = models.ForeignKey(Caja, on_delete=models.CASCADE, db_column='NUMERO_CAJA')
    atencion = models.ForeignKey('atencion.Atencion', on_delete=models.CASCADE)

    def __str__(self):
        return self.id

class Boleta(models.Model):
    codigo_sii = models.CharField(max_length=50, unique=True, db_column='CODIGO_SII')
    fecha_emision = models.DateTimeField(auto_now=True, db_column='FECHA_EMISION')
    total = models.DecimalField(max_digits=9, decimal_places=2)
    pago = models.ForeignKey(Pago, on_delete=models.CASCADE)

    def __str__(self):
        return self.codigoSii