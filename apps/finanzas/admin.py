from django.contrib import admin
from .models import MedioPago, Caja, Pago, Boleta

# Register your models here.
admin.site.register(Caja)
admin.site.register(Pago)
admin.site.register(Boleta)
admin.site.register(MedioPago)