from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.Atencion)
admin.site.register(models.Pedido)