from django.db import models
import json

PEDIDO_ESTADOS = (
   ('ORDENADO', 'Ordenado'),
   ('PENDIENTE', 'Pendiente'),
   ('PREPARADO', 'Preparado'),
   ('ENTREGADO', 'Entregado'),
   ('CANCELADO', 'Cancelado'),
)

# Create your models here.
class Atencion(models.Model):
    codigo = models.CharField(max_length=150)
    total = models.DecimalField(max_digits=9, decimal_places=2, null=True)
    fecha = models.DateTimeField(auto_now=True)
    esta_activo = models.BooleanField(default=True)
    numero_mesa = models.ForeignKey('core.Mesa', on_delete=models.CASCADE, db_column='NUMERO_MESA')
    cliente = models.ForeignKey('core.Cliente', on_delete=models.CASCADE)
    esta_pagada = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = "Atenciones"

    def __str__(self):
        return self.codigo

    @property
    def json(self):
        return json.dumps(self)

class Pedido(models.Model):
    garzon = models.ForeignKey('core.User', on_delete=models.CASCADE, related_name='garzon_pedido')
    estado = models.CharField(max_length=20, choices=PEDIDO_ESTADOS, default='ORDENADO')
    fecha_ingreso = models.DateTimeField(auto_now=True, db_column='FECHA_INGRESO')
    fecha_preparacion = models.DateTimeField(auto_now=False, null=True, db_column='FECHA_PREPARACION')
    fecha_entrega = models.DateTimeField(auto_now=False, null=True, db_column='FECHA_ENTREGA', blank=True)
    atencion = models.ForeignKey(Atencion, on_delete=models.CASCADE)

    def __str__(self):
        return "{}-{}".format(self.atencion, self.id)

class PedidoReceta(models.Model):
    pedido = models.ForeignKey(Pedido, on_delete=models.CASCADE)
    receta = models.ForeignKey('productos.Receta', on_delete=models.CASCADE)
    cantidad = models.PositiveIntegerField(db_column='CANTIDAD')
    comentario = models.PositiveIntegerField(db_column='COMENTARIO', null=True, blank=True)

    class Meta:
        db_table = 'ATENCION_PEDIDO_RECETA'